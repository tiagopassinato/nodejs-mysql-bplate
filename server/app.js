/*
 *       _    _      _____         
 *      / \  (_)_ __|  ___|____  __
 *     / _ \ | | '__| |_ / _ \ \/ /
 *    / ___ \| | |  |  _| (_) >  < 
 *   /_/   \_\_|_|  |_|  \___/_/\_\
 *             Wallet Micro-Service
 */

// Dependencies 
var express = require('express');
var path = require('path');
var logger = require('morgan');
var http = require('http');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('config');


// *** routes *** //
var routes = require('./routes/index.js');

// *** express instance *** //
var app = express();


// *** Mysql connection *** ///
var db = require('knex')(config.get('DB'));

db.table('users').then();

// *** middleware *** //
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../client/public')));

// *** main routes *** //
app.use('/', routes);

// *** server config *** //
var server = http.createServer(app);
server.listen(3000, function() {
    console.log("Airfox Wallet-Service running on port 3000");
});

module.exports = app;