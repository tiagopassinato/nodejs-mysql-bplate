# README #

Simple NodeJS boilerplate with:

* SQL connectivity (Knex);
* Environment based Config files;
* Express Routes;
* Mocha/Chai Unit test examples;